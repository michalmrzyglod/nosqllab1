#!/usr/bin/python

import pymongo,time,string,random

def main():
    ### Connection to Mongo
    mcon1 = pymongo.MongoClient("mongodb://root:root@192.168.30.2:27017/")
    db1 = mcon1['lab']
    col1 = db1['wojewodztwa']

    mcon2 = pymongo.MongoClient("mongodb://root:root@192.168.30.3:27017/")
    db2 = mcon2['lab']
    col2 = db2['wojewodztwa']
    col2.drop()
    
    mongo1_count = col1.count_documents({})
    mongo2_count = col2.count_documents({})

    print("Mongo 1 count: %s" % mongo1_count)
    print("Mongo 2 count(should be empty): %s" % mongo2_count)

    counter = 0
    start = time.time()

    while mongo2_count < mongo1_count:
      rand = random.randint(0, mongo1_count-1)
      if col2.count_documents({ '_id': rand }, limit = 1) != 0:
        counter=counter+1
      else:
        result = col1.find_one({ "_id": rand })
        col2.insert_one(result)
        mongo2_count += 1

    print('Test duration: ', time.time()-start, ' seconds.')
    print('Waste counter : ', counter, ' times.')

    print("Mongo 1 count: %s" % mongo1_count)
    print("Mongo 2 count: %s" % col2.count_documents({}))

main()