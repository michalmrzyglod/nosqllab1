#!/bin/bash

## clean containers
docker-compose -f lab1.yml down
docker-compose -f lab1.yml kill

# Run containers and seed database with data
docker-compose -f lab1.yml up -d
echo "Running containers"
python3 table_gen.py
#Wait while maraidb fully initialuize
sleep 50
docker exec -i mariadb_container chmod +x /mnt/import.sh
docker exec -i mariadb_container ./mnt/import.sh
sleep 5
docker exec -it python_container python /mnt/lab1.py
